# Data science hiring

This is an intro data science task for candidates

## Instructions

### Task

Your task is to create an algorithm, that takes html page as input and infers if the page contains the information about `cancer tumorbaord` or not. 
What is a tumor board? Tumor Board is a consilium of doctors (usually from diferent disciplines) discussing cancer cases in their deprtments. If you want to know more please read this [article](https://www.cancer.net/blog/2017-07/what-tumor-board-expert-qa)


We expect from you `submission.csv` file for test data with columns [`doc_id` and `prediction`] and jupyter notebook with code and next documentation:
* How did you decide to handle this amount of data
  - the amount of data was both large and small. It took a lot of space, but had a small number of categories. I've used in-place data generator for training the model, which only held file names and target classes in memory and loaded required files on demand using batches, so no more than *batch_size* files were loaded at once. The small amount of samples was solved using one shot learning, which does not attempt to learn a classification pattern, but a similarity function, were samples from one class are trained to be similar and those from different classes are trained to be dissimilar.
* How did you decide to do feature engineering
  - Feature engineering was done using TF-IDF vectorizer, since I could not find good enough pre-trained German embedding quickly enough. For English language, embedding model such as BERT could be used. I also did quite heavy preprocessing, since models based on word frequency are usually better with more preprocessing. I did text cleaning with beautiful soup, processing with gensim, removed German stopwords and stemmed words. 
* How did you decide which models to try (if you decide to train any models)
  - I used benchmark model, which was typical SVC, which is usually good benchmark model and simplified siamese network. I decided to use those, because siamese net should do well with limited number of examples and SVC is a good baseline to compare, if one shot approach does give a visible benefit.
* How did you perform validation of your model
  - using 30% of test data and comparing metrics. For 3 classes only, confusion matrix contains all information and is easily readable.
* What metrics did you measure
  - during training binary accuracy, since there was not a lot of data, so I decided not to create a validation set. During testing I mostly used confusion matrix, for a single metric f-score is best quantifier of classification quality.
* How do you expect your model to perform on test data (in terms of your metrics)
  - I hope to achieve f-score over 0.8 for class 2, over 0.6 for class 1 and hopefully anything over 0 for class 3. In terms of mis-classifications model handles class 3 best, so it will probably miss most of test examples of this class. It should make a small amount of mistakes between classes 1 and 2, but probably no more than 30% of samples.
* How fast will your algorithm performs and how could you improve its performance if you would have more time
  - it performs reasonably well in term of computation speed. For a fitted model weight pruning and model optimization could be done to make it faster. I think performance in terms of metrics is crucial and should come before computational performance, so I would focus mostly on improving the model quality in terms of metrics, not performance.
* How do you think you would be able to improve your algorithm if you would have more data
  - if there was a lot more data, it could use traditional classification approach. One improvement would be to use pre-trained encoder for text features. Training from scratch using RNN encoder instead of TF-IDF could be a big improvement, but it would create a problem of variable length of web pages.
* What potential issues do you see with your algorithm
  - it relies heavily on the data used to train it, so it will probably have massive overfitting issues. For test data, it should perform reasonably well, but one shot learning is not great at generalization.

`Note`: if you would like to go an extra mile in this task try to identify  tumor board types `interdisciplinary`, `breast` and any third type of tumor board up to you. 
For these tumor boards please try to identify their schedule: Day (e.g. Friday), frequency (e.g. weekly, bi-weekly, monthly) and time when they start.

### Data

You have `train.csv` and `test.csv` files and folder with corresponding `.html` files.

Files:
* `train.csv` contains next columns: `url`, `doc_id` and `label`
* `test.csv` contains next columns: `url` and `doc_id`
* `htmls` contains files with names `{doc_id}.html`
* `keyword2tumor_type.csv` contains useful keywords for types of tumorboards

Description of tumorboard labels:
* 1 (no evidence): tumorboards are not mentioned on the page
* 2 (medium confidence): tumorboards are mentioned, but the page is not completely dedicated to tumorboard description
* 3 (high confidence): page is completely dedicated to description of tumorboardd types and dates

You are asked to prepare a model using htmls, referred in `train.csv` and make predictions for htmls from `test.csv`

### Tips

* to extract clean text from the page you can use BeautifulSoup module like this
```python
from bs import BeautifulSoup

content = read_html()
soup = BeautifulSoup(content)
clean_text = soup.get_text(' ')
```

* If you decide that you don't need, for example tags `<p>` in your document, you can do this:

```python
from bs import BeautifulSoup

content = read_html()
soup = BeautifulSoup(content)
for tag in soup.find_all('p'):
    tag.decompose()

```

# Submission 

Write your code in the develop branch and submit a MergeRequest to @M_Feldman to us once you're done. In case of questions, add a ticket to your GitLab Issue board and assign @M_Feldman